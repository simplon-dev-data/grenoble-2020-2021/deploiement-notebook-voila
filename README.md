# Deploiement Notebook Voilà

Il existe différentes manières de rentre accessible en ligne un Jupyter Notebook :

![schema](schema.png)

## Le moins interactif : la conversion en HTML

Le création d'un Notebook statique (à l'aide de `nbconvert`) déployé sur Gitlab Pages a été réalisée dans l'activité suivante : https://gitlab.com/simplon-dev-data/grenoble-2020-2021/deploiement-notebook

Dans le cas où votre Notebook Jupyter (fichier .ipynb) est publique et est hébergé sur **Github**, le site https://nbviewer.jupyter.org/ vous permet de déployer le Notebook en statique directement.

### Activité

- Essayer le déploiement d'un notebook Jupyter de Github (soit un des votres, soit celui-ci https://github.com/python-visualization/folium/blob/master/examples/GeoJSON_and_choropleth.ipynb) à l'aide de **nbviewer**.

> Quelles sont les différences entre la visualisation dans Github et sur Nbviewer ?

## Plus intéractif mais sans ajout de code possible : voilà

La bibliothèque Voilà permet de déployer un Jupyter Notebook avec son noyau Python mais sans permettre de modification du code.

### Activité

- Créer un environnement virtuel à partir du fichier Pipfile

- Lancer le Jupyter Notebook `Data_widgets.ipynb` et étudier le fonctionnement des widgets

- Lancer la commande `voila Data_widgets.ipynb` (https://voila.readthedocs.io/en/stable/)

> Quelles sont les différences avec le Jupyter Notebook ?
> A quels cas d'usage répond l'utilisation de Voilà ?

- Réaliser le déploiement avec Heroku en suivant la documentation de Voilà : https://voila.readthedocs.io/en/stable/deploy.html#deployment-on-heroku

## Le plus interactif : colaboratory

Pour pouvoir intéragir et modifier du code en ligne dans un Jupyter Notebook, le plus pratique est d'utiliser les services de Cloud comme Colaboratory (accessible dans Google Drive) : https://colab.research.google.com/

## Ressources

- nbconvert : https://nbconvert.readthedocs.io/en/latest/

- nbviewer : https://nbviewer.jupyter.org/

- Voilà : https://voila.readthedocs.io/en/stable/

- mybinder : https://mybinder.org/

- Colaboratory : https://colab.research.google.com/

- Conférence - Dashboarding with Jupyter Notebooks, Voila and Widgets | SciPy 2019 | M. Breddels and M. Renou : https://www.youtube.com/watch?v=VtchVpoSdoQ
